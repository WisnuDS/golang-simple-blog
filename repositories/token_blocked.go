package repositories

import (
	"blog.example.com/v1/configs"
	"blog.example.com/v1/models"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type TokenBlockedRepository struct {
	db *mongo.Database
}

func NewTokenBlockedRepository() *TokenBlockedRepository {
	return &TokenBlockedRepository{
		db: configs.GetConnection(),
	}
}

func (t *TokenBlockedRepository) FindBlockedToken(token string) (*models.TokenBlocked, error) {
	var model models.TokenBlocked
	err := t.db.Collection("tokens_blocked").FindOne(context.TODO(), bson.M{"token": token}).Decode(&model)
	if err != nil {
		return nil, err
	}

	return &model, nil
}

func (t *TokenBlockedRepository) Create(token string) error {
	model := models.TokenBlocked{
		Token: token,
	}

	_, err := t.db.Collection("tokens_blocked").InsertOne(context.TODO(), model)
	if err != nil {
		return err
	}

	return nil
}
