package repositories

import (
	"blog.example.com/v1/configs"
	"blog.example.com/v1/models"
	"blog.example.com/v1/utils"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type BlogRepository struct {
	db *mongo.Database
}

func NewBlogRepository() *BlogRepository {
	return &BlogRepository{
		db: configs.GetConnection(),
	}
}

func (b *BlogRepository) GetAllBlog() ([]models.BlogModel, error) {
	cursor, err := b.db.Collection("blog").Find(context.TODO(), bson.M{})
	if err != nil {
		return nil, err
	}

	var blogs []models.BlogModel
	err = cursor.All(context.TODO(), &blogs)
	if err != nil {
		return nil, err
	}

	return blogs, nil

}

func (b *BlogRepository) GetAllAndCount() (*[]bson.M, error) {
	var author []bson.M
	group := bson.D{
		{"$group", bson.D{
			{"_id", "$author"},
			{"count", bson.D{{"$sum", 1}}},
		}},
	}

	result, err := b.db.Collection("blog").Aggregate(context.TODO(), mongo.Pipeline{
		group,
	})
	if err != nil {
		return nil, err
	}

	err = result.All(context.TODO(), &author)
	if err != nil {
		return nil, err
	}

	return &author, nil

}

func (b *BlogRepository) GetAndPaginate(page string, limit string, sort string, dir string, search string) (*interface{}, error) {
	collection := b.db.Collection("blog")

	var blogs []models.BlogModel
	data, err := utils.NewPagination(context.TODO(), collection, page, limit, sort, dir, search, models.BlogModel{}).Paginate(blogs)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (b *BlogRepository) GetBlogBySlug(slug string) (*models.BlogModel, error) {
	var blog models.BlogModel
	err := b.db.Collection("blog").FindOne(context.TODO(), bson.M{"slug": slug}).Decode(&blog)
	if err != nil {
		return nil, err
	}

	return &blog, nil
}

func (b *BlogRepository) GetBlogById(id string) (*models.BlogModel, error) {
	var blog models.BlogModel
	_id, _ := primitive.ObjectIDFromHex(id)
	err := b.db.Collection("blog").FindOne(context.TODO(), bson.M{"_id": _id}).Decode(&blog)
	if err != nil {
		return nil, err
	}

	return &blog, nil
}

func (b *BlogRepository) Create(title string, description string, author string, cover string, slug string) (*models.BlogModel, error) {
	authorId, _ := primitive.ObjectIDFromHex(author)
	data := models.BlogModel{
		Title: title,
		Description: description,
		Author: authorId,
		Cover: cover,
		Slug: slug,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	result, err := b.db.Collection("blog").InsertOne(context.TODO(), data)
	if err != nil {
		return nil, err
	}

	data.Id = result.InsertedID.(primitive.ObjectID)

	return &data, nil
}

func (b *BlogRepository) Delete(slug string) error {
	_, err := b.db.Collection("blog").DeleteOne(context.TODO(), bson.M{"slug": slug})
	if err != nil {
		return err
	}

	return nil
}

func (b *BlogRepository) Update(id string, title string, description string, author string, cover string, slug string) (*models.BlogModel, error) {
	authorId, _ := primitive.ObjectIDFromHex(author)
	data, err := b.GetBlogById(id)
	if err != nil {
		return nil, err
	}
	data.Title = title
	data.Description = description
	data.Author = authorId
	data.Cover = cover
	data.Slug = slug
	data.UpdatedAt = time.Now()

	objectId, _ := primitive.ObjectIDFromHex(id)

	doc, err := toDoc(data)

	_, err = b.db.Collection("blog").UpdateOne(context.TODO(), bson.M{"_id": objectId}, bson.D{{"$set", doc}})
	if err != nil {
		return nil, err
	}

	return data, nil
}

func toDoc(v interface{}) (doc *bson.D, err error) {
	data, err := bson.Marshal(v)
	if err != nil {
		return
	}

	err = bson.Unmarshal(data, &doc)
	return
}