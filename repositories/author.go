package repositories

import (
	"blog.example.com/v1/configs"
	"blog.example.com/v1/models"
	"blog.example.com/v1/utils"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type AuthorRepository struct {
	db *mongo.Database
}

func NewAuthorRepository() *AuthorRepository {
	return &AuthorRepository{
		db: configs.GetConnection(),
	}
}

func (a *AuthorRepository) GetAllAndCount() (*[]bson.M, error) {
	var author []bson.M
	group := bson.D{
		{"$group", bson.D{
			{"_id", "$author"},
			{"count", bson.D{{"$count", "$_id"}}},
		}},
	}

	result, err := a.db.Collection("author").Aggregate(context.TODO(), mongo.Pipeline{
		group,
	})
	if err != nil {
		return nil, err
	}

	err = result.All(context.TODO(), &author)
	if err != nil {
		return nil, err
	}

	return &author, nil

}

func (a *AuthorRepository) GetById(id string) (*[]bson.M, error) {
	var author []bson.M
	_id, _ := primitive.ObjectIDFromHex(id)
	lookup := bson.D{
		{"$lookup", bson.D{
			{"from", "blog"},
			{"localField", "blogs"},
			{"foreignField", "_id"},
			{"as", "blogs"},
		}},
	}

	match := bson.D{
		{
			"$match",
			bson.D{
				{"_id", _id},
			},
		},
	}

	result, err := a.db.Collection("author").Aggregate(context.TODO(), mongo.Pipeline{
		lookup,
		match,
	})
	if err != nil {
		return nil, err
	}

	err = result.All(context.TODO(), &author)
	if err != nil {
		return nil, err
	}

	return &author, nil
}

func (a *AuthorRepository) Create(name string, description string, blogs []string, email string, password string) (*models.AuthorModel, error) {
	enc, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	data := models.AuthorModel{
		Name:        name,
		Description: description,
		Blogs:       utils.GenerateToObjectId(blogs),
		Email:       email,
		Password:    string(enc),
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	result, err := a.db.Collection("author").InsertOne(context.TODO(), data)
	if err != nil {
		return nil, err
	}

	data.Id = result.InsertedID.(primitive.ObjectID)

	return &data, nil
}

func (a *AuthorRepository) GetByEmail(email string) (*models.AuthorModel, error) {
	var user models.AuthorModel
	if err := a.db.Collection("author").FindOne(
		context.TODO(),
		bson.M{"email": email}).
		Decode(&user); err != nil {
		return nil, err
	}

	return &user, nil
}
