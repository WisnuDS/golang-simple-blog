package middlewares

import (
	"blog.example.com/v1/configs"
	"blog.example.com/v1/models"
	"blog.example.com/v1/repositories"
	"blog.example.com/v1/utils"
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"strings"
)

func AuthMiddleware() gin.HandlerFunc{
	return func (ctx *gin.Context) {
		firebaseId := ctx.GetHeader("FirebaseId")
		repo := repositories.NewTokenBlockedRepository()

		if firebaseId != "" {
			firebaseClaim, err := firebaseValidation(firebaseId)
			if err != nil {
				token, _ := repo.FindBlockedToken(firebaseId)
				if token != nil {
					ctx.AbortWithStatusJSON(403, gin.H{
						"code": 400,
						"message": "Token expired",
					})
					return
				}

				ctx.AbortWithStatusJSON(403, gin.H{
					"code": 400,
					"message": err.Error(),
				})
				return
			}

			var user models.AuthorModel
			byteData, _ := json.Marshal(firebaseClaim["User"])
			err = json.Unmarshal(byteData, &user)
			if err != nil {
				return
			}

			ctx.Set("user", user)
			ctx.Next()
		} else {
			header := ctx.GetHeader("Authorization")

			if header == "" {
				ctx.AbortWithStatusJSON(403, gin.H{
					"code": 403,
					"message": "Unauthorization user",
				})
				return
			}

			arr := strings.Split(header, " ")
			if arr[0] != "Bearer" {
				ctx.AbortWithStatusJSON(403, gin.H{
					"code": 400,
					"message": "Invalid token",
				})
				return
			}

			token, _ := repo.FindBlockedToken(arr[1])
			if token != nil {
				ctx.AbortWithStatusJSON(403, gin.H{
					"code": 400,
					"message": "Token expired",
				})
				return
			}

			claim, err := utils.ValidateToken(arr[1])
			if err != nil {
				if err := repo.Create(arr[1]); err != nil {
					ctx.AbortWithStatusJSON(403, gin.H{
						"code": 400,
						"message": err.Error(),
					})
					return
				}

			}

			ctx.Set("user", claim.User)
			ctx.Next()
		}
	}
}

func firebaseValidation(token string) (map[string] interface{}, error) {
	firebase := configs.GetFirebaseClient()
	idToken, err := firebase.VerifyIDToken(context.Background(), token)
	if err != nil {
		return nil, err
	}

	return idToken.Claims, nil
}
