package utils

import (
	"context"
	"fmt"
	mongopagination "github.com/gobeam/mongo-go-pagination"
	"github.com/json-iterator/go/extra"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"reflect"
	"strconv"
)

type Params struct {
	Context    context.Context
	Collection *mongo.Collection
	Page       string
	Limit      string
	Search     string
	Sort       string
	Dir        string
	Model      interface{}
}

func NewPagination(
	context context.Context,
	collection *mongo.Collection,
	page string,
	limit string,
	sort string,
	dir string,
	search string,
	model interface{},
) *Params {
	return &Params{
		Context:    context,
		Collection: collection,
		Page:       page,
		Limit:      limit,
		Sort:       sort,
		Dir:        dir,
		Search:     search,
		Model:      model,
	}
}

func (p *Params) Paginate(decode interface{}) (*interface{}, error) {
	var page, limit, dir int64

	if p.Page == "" {
		page = 1
	} else {
		conv, err := strconv.Atoi(p.Page)
		if err != nil {
			return nil, err
		}
		page = int64(conv)
	}

	if p.Limit == "" {
		limit = 2
	} else {
		conv, err := strconv.Atoi(p.Limit)
		if err != nil {
			return nil, err
		}
		limit = int64(conv)
	}

	if p.Sort == "" {
		p.Sort = "_id"
	}

	if "asc" == extra.LowerCaseWithUnderscores(p.Dir) {
		dir = 1
	} else {
		dir = -1
	}

	filter :=  bson.D{}
	if p.Search != "" {
		ell := bson.A{}
		v := reflect.ValueOf(p.Model)
		typeOf := v.Type()
		reg := primitive.Regex{Pattern: fmt.Sprintf(".*%s.*", p.Search)}
		for i := 0; i < v.NumField(); i++ {
			ell = append(ell, bson.M{typeOf.Field(i).Tag.Get("bson"): reg})
		}
		filter = bson.D{{"$or", ell}}
	}

	if _, err := mongopagination.
		New(p.Collection).
		Context(p.Context).
		Page(page).
		Limit(limit).
		Sort(p.Sort, dir).
		Decode(&decode).
		Filter(filter).
		Find(); err != nil {
		return nil, err
	}

	return &decode, nil
}
