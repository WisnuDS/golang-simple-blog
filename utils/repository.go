package utils

import "go.mongodb.org/mongo-driver/bson/primitive"

func GenerateToObjectId(ids []string) []primitive.ObjectID {
	var a []primitive.ObjectID
	for i := range ids {
		obj, _ := primitive.ObjectIDFromHex(ids[i])
		a = append(a, obj)
	}

	return a
}
