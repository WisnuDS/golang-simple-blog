package utils

import (
	"blog.example.com/v1/models"
	"fmt"
	"github.com/golang-jwt/jwt"
	"os"
	"strconv"
	"time"
)

type CustomClaim struct {
	User *models.AuthorModel
	jwt.StandardClaims
}

func GenerateToken(user *models.AuthorModel, refresh bool) (string, error) {
	secret := os.Getenv("SECRET_JWT_KEY")
	if secret == "" {
		secret = "secret"
	}

	var expired string
	if refresh {
		expired = os.Getenv("REFRESH_TOKEN_EXPIRED_TIME")
	} else {
		expired = os.Getenv("ACCESS_TOKEN_EXPIRED_TIME")
	}

	e, err := strconv.ParseInt(expired, 10, 32)
	if err != nil {
		return "", err
	}


	withClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, CustomClaim{
		user,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Minute * time.Duration(e)).Unix(),
		},
	})

	t, err := withClaims.SignedString([]byte(secret))

	if err != nil {
		return "", err
	}

	return t, nil
}

func ValidateToken (token string) (*CustomClaim, error) {
	secret := os.Getenv("SECRET_JWT_KEY")
	if secret == "" {
		 secret = "secret"
	}

	jwtToken, err := jwt.ParseWithClaims(token, &CustomClaim{} , func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, fmt.Errorf("invalid signing method")
		}
		return []byte(secret), nil
	})

	if err != nil {
		return nil, err
	}

	return jwtToken.Claims.(*CustomClaim), nil
}

