package routers

import (
	"blog.example.com/v1/handlers"
	"github.com/gin-gonic/gin"
)

func RegisterAuthRouter (route *gin.RouterGroup) {
	auth := route.Group("/auth")
	{
		auth.POST("/login", handlers.Login)
		auth.POST("/login/firebase", handlers.LoginWithFirebase)
		auth.POST("/logout", handlers.Logout)
		auth.POST("/refresh-token", handlers.RefreshToken)
	}
}
