package routers

import "github.com/gin-gonic/gin"

func Init() *gin.Engine {
	router := gin.Default()

	router.GET("/", func(context *gin.Context) {
		context.JSONP(200, gin.H{
			"code" : 200,
			"message" : "https://www.youtube.com/watch?v=dQw4w9WgXcQ&t=9s",
		})
	})

	api := router.Group("/api")
	{
		RegisterBlogRouter(api)
		RegisterAuthorRouter(api)
		RegisterAuthRouter(api)
		RegisterNotificationRouter(api)
	}

	return router
}
