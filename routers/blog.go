package routers

import (
	"blog.example.com/v1/handlers"
	"blog.example.com/v1/middlewares"
	"github.com/gin-gonic/gin"
)

func RegisterBlogRouter(route *gin.RouterGroup)  {
	blog := route.Group("/blog")
	blog.Use(middlewares.AuthMiddleware())
	{
		blog.GET("", handlers.ReadAllBlog)
		blog.GET("/:slug", handlers.ReadBlog)
		blog.POST("/create", handlers.CreateBlog)
		blog.PUT("/update/:id", handlers.UpdateBlog)
		blog.DELETE("/delete/:slug", handlers.DeleteBlog)
	}
}
