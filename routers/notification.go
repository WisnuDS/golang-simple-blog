package routers

import (
	"blog.example.com/v1/handlers"
	"github.com/gin-gonic/gin"
)

func RegisterNotificationRouter (route *gin.RouterGroup) {
	notification := route.Group("/notification")
	{
		notification.POST("/send", handlers.SendNotification)
	}
}

