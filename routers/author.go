package routers

import (
	"blog.example.com/v1/handlers"
	"blog.example.com/v1/middlewares"
	"github.com/gin-gonic/gin"
)

func RegisterAuthorRouter(route *gin.RouterGroup)  {
	author := route.Group("/author")
	author.Use(middlewares.AuthMiddleware())
	{
		author.GET("", handlers.ReadAll)
		author.GET("/:id", handlers.ReadAuthor)
		author.POST("/create", handlers.CreateAuthor)
	}
}
