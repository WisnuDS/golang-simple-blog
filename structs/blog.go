package structs

type CreateRequest struct {
	Title       string `validate:"required" json:"title"`
	Description string `validate:"required" json:"description"`
	Author      string `validate:"required" json:"author"`
	Cover       string `validate:"required" json:"cover"`
	Slug        string `validate:"required" json:"slug"`
}
