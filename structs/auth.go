package structs

import "blog.example.com/v1/models"

type LoginRequest struct {
	Email    string `validate:"required,email" json:"email"`
	Password string `validate:"required,gte=8" json:"password"`
}

type LoginResponse struct {
	Token        string              `json:"token"`
	RefreshToken string              `json:"refresh_token"`
	User         *models.AuthorModel `json:"user"`
}

type RefreshTokenRequest struct {
	Token string `json:"token"`
}

type LogoutRequest struct{
	Token string `json:"token"`
	RefreshToken string `json:"refresh_token"`
}

type Void struct {}