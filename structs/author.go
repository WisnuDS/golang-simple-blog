package structs

type AuthorCreateRequest struct {
	Name        string   `validate:"required" json:"name"`
	Description string   `validate:"required" json:"description"`
	Blogs       []string `json:"blogs"`
	Email       string   `validate:"required,email" json:"email"`
	Password    string   `validate:"required,gte=8"`
}
