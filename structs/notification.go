package structs

type NotificationRequest struct {
	Email string
	Message string
}

type NotificationResponse struct {}