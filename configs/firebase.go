package configs

import (
	"context"
	firebase "firebase.google.com/go"
	"firebase.google.com/go/auth"
	"fmt"
	"google.golang.org/api/option"
	"os"
	"path/filepath"
)

var firebaseClient *auth.Client

func init() {
	if client == nil {
		path := os.Getenv("KEY_PATH")
		if path == "" {
			path = "storages/keys/arvis-blog-firebase-adminsdk-ewg4j-53657b6f7c.json"
		}
		keyPath, err := filepath.Abs("./"+path)
		if err != nil {
			fmt.Println("Error when create path")
		}

		opt := option.WithCredentialsFile(keyPath)

		app, err := firebase.NewApp(context.Background(), nil, opt)
		if err != nil {
			fmt.Println("Error when start firebase")
		}

		authentication, err := app.Auth(context.Background())
		if err != nil {
			fmt.Println("Error when start authentication service")
		}

		firebaseClient = authentication
	}
}

func GetFirebaseClient() *auth.Client {
	return firebaseClient
}
