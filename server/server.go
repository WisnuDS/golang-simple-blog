package server

import (
	_ "blog.example.com/v1/docs"
	"blog.example.com/v1/routers"
	"fmt"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"net/http"
	"os"
)

func Init() error {
	port := os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}

	router := routers.Init()

	s := &http.Server{
		Addr: fmt.Sprintf(":%s", port),
		Handler: router,
	}

	if os.Getenv("APP_MODE") != "production" {
		router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}

	if err := s.ListenAndServe(); err != nil {
		return err
	}

	fmt.Printf("App running on port: %s", port)
	return nil
}
