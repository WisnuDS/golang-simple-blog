package main

import (
	"blog.example.com/v1/configs"
	"blog.example.com/v1/server"
	"github.com/joho/godotenv"
)

// @title           Swagger Blog API
// @version         1.0
// @description     This is a sample server celler server.
// @termsOfService  http://swagger.io/terms/

// @contact.name   API Support
// @contact.url    https://github.com/WisnuDs
// @contact.email  support@swagger.io

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html

// @host      127.0.0.1:3000
// @BasePath  /api

// @securityDefinitions.apikey  ApiKeyAuth
// @in                          header
// @name                        Authorization

func main() {
	if err := godotenv.Load(); err != nil {
		panic(err)
	}
	defer func() {
		err := configs.CloseConnection()
		if err != nil {
			panic(err)
		}
	}()
	if err := server.Init();err != nil {
		panic(err)
	}
}
