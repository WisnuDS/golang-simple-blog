package publisher

import (
	"blog.example.com/v1/configs"
	"encoding/json"
	"github.com/streadway/amqp"
)

type Publisher struct {
	channel *amqp.Channel
	queues  map[string]*amqp.Queue
}

type QueueParam struct {
	Name       string
	Durable    bool
	AutoDelete bool
	Exclusive  bool
	NoWait bool
	Args   amqp.Table
}

func NewPublisher() *Publisher {
	return &Publisher{
		channel: configs.GetChannel(),
		queues:  make(map[string]*amqp.Queue),
	}
}

func (p *Publisher) CreateNewQueue(param QueueParam) (*amqp.Queue, error) {
	queue, err := p.channel.QueueDeclare(
		param.Name,
		param.Durable,
		param.AutoDelete,
		param.Exclusive,
		param.NoWait,
		param.Args,
	)
	if err != nil {
		return nil, err
	}

	p.queues[queue.Name] = &queue

	return &queue, nil
}

func (p *Publisher) PublishMessage(queueName string, message interface{}) error {
	msg, _ := json.Marshal(message)
	err := p.channel.Publish(
		"",
		queueName,
		false,
		false,
		amqp.Publishing{
			ContentType: "plain/text",
			Body:        msg,
		},
	)
	if err != nil {
		return err
	}

	return nil
}
