package validators

import (
	"blog.example.com/v1/repositories"
	"blog.example.com/v1/structs"
	"fmt"
	"github.com/go-playground/validator/v10"
)

type ValidatorAuthor struct {
	validator *validator.Validate
	repository *repositories.AuthorRepository
}

func NewAuthorValidator() *ValidatorAuthor {
	return &ValidatorAuthor{
		validator: validator.New(),
		repository: repositories.NewAuthorRepository(),
	}
}

func (v *ValidatorAuthor) ValidateAuthorCreateRequest(request *structs.AuthorCreateRequest) error {
	if err := v.validator.Struct(request); err != nil {
		return err
	}

	user, err := v.repository.GetByEmail(request.Email)
	if err != nil {
		return err
	}

	if user != nil {
		return fmt.Errorf("email has been taken, use another email")
	}

	return nil
}
