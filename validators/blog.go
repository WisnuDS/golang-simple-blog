package validators

import (
	"blog.example.com/v1/repositories"
	"blog.example.com/v1/structs"
	"fmt"
	"github.com/go-playground/validator/v10"
)

type ValidatorBlog struct {
	validator *validator.Validate
	repository *repositories.BlogRepository
}

func NewBlogValidator() *ValidatorBlog {
	return &ValidatorBlog{
		validator: validator.New(),
		repository: repositories.NewBlogRepository(),
	}
}

func (v *ValidatorBlog) ValidateCreateRequest(request *structs.CreateRequest) error {
	if err := v.validator.Struct(request); err != nil {
		return err
	}

	if data, _ := v.repository.GetBlogBySlug(request.Slug); data != nil {
		return fmt.Errorf("slug already exists")
	}

	return nil
}

func (v *ValidatorBlog) ValidateDeleteRequest(slug string) error {
	if data, _ := v.repository.GetBlogBySlug(slug); data == nil {
		return fmt.Errorf("slug not exists")
	}

	return nil
}
