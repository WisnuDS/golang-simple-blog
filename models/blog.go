package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type BlogModel struct {
	Id          primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Title       string             `bson:"title" json:"title"`
	Description string             `bson:"description" json:"description"`
	Author      primitive.ObjectID `bson:"author" json:"author"`
	Cover       string             `bson:"cover" json:"cover"`
	Slug        string             `bson:"slug" json:"slug"`
	CreatedAt   time.Time          `bson:"created_at" json:"created_at"`
	UpdatedAt   time.Time          `bson:"updated_at" json:"updated_at"`
}
