package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type AuthorModel struct {
	Id          primitive.ObjectID   `bson:"_id,omitempty" json:"id"`
	Name        string               `bson:"name" json:"name"`
	Email       string               `bson:"email" json:"email"`
	Password    string               `bson:"password" json:"password"`
	Description string               `bson:"description" json:"description"`
	Blogs       []primitive.ObjectID `bson:"blogs" json:"blogs"`
	FirebaseId  string               `bson:"firebase_id" json:"firebase_id"`
	CreatedAt   time.Time            `bson:"created_at" json:"created_at"`
	UpdatedAt   time.Time            `bson:"updated_at" json:"updated_at"`
}
