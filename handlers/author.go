package handlers

import (
	"blog.example.com/v1/services"
	"blog.example.com/v1/structs"
	"blog.example.com/v1/validators"
	"github.com/gin-gonic/gin"
)

func ReadAuthor(ctx *gin.Context)  {
	id := ctx.Param("id")
	service := services.NewAuthorService()
	author, err := service.ReadService(id)
	if err != nil {
		serverError(ctx, err.Error())
		return
	}

	successResponse(ctx, "Success get all blog data", author)
}

func CreateAuthor(ctx *gin.Context) {
	var body structs.AuthorCreateRequest
	if err := ctx.BindJSON(&body); err != nil {
		badRequest(ctx, "Error")
		return
	}

	validator := validators.NewAuthorValidator()
	if err := validator.ValidateAuthorCreateRequest(&body); err != nil {
		badRequest(ctx, err.Error())
		return
	}

	service := services.NewAuthorService()
	data, err := service.CreateService(&body)
	if err != nil {
		serverError(ctx, err.Error())
		return
	}

	successResponse(ctx, "Succes Create Data", data)
}
