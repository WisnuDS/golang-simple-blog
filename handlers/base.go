package handlers

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func successResponse(ctx *gin.Context, message string, data interface{}) {
	ctx.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"message": message,
		"data": data,
	})
}

func badRequest(ctx *gin.Context, message string) {
	ctx.JSON(http.StatusBadRequest, gin.H{
		"code": http.StatusBadRequest,
		"message": message,
	})
}

func serverError(ctx *gin.Context, message string) {
	ctx.JSON(http.StatusInternalServerError, gin.H{
		"code": http.StatusInternalServerError,
		"message": message,
	})
}
