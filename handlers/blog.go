package handlers

import (
	"blog.example.com/v1/services"
	"blog.example.com/v1/structs"
	"blog.example.com/v1/validators"
	"github.com/gin-gonic/gin"
)

// ReadAllBlog
// @Summary      Show all blogs
// @Description  Show you all of blogs
// @Tags         Blog
// @Accept       json
// @Produce      json
// @Success      200  {object}  models.BlogModel
// @Failure      400  {object}  httputils.HTTPError
// @Failure      401  {object}  httputils.HTTPError
// @Failure      404  {object}  httputils.HTTPError
// @Failure      500  {object}  httputils.HTTPError
// @Security     ApiKeyAuth
// @Router       /blog [get]
func ReadAllBlog(ctx *gin.Context) {
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	sort := ctx.Query("sort")
	dir := ctx.Query("dir")
	search := ctx.Query("search")

	service := services.NewBlogService()
	blogs, err := service.ReadALlService(page, limit, sort, dir, search)
	if err != nil {
		serverError(ctx, err.Error())
		return
	}

	successResponse(ctx, "Success get all blog data", blogs)
}

func ReadAll(ctx *gin.Context) {
	service := services.NewBlogService()
	author, err := service.ReadAllAndCount()
	if err != nil {
		serverError(ctx, err.Error())
		return
	}

	successResponse(ctx, "Success get all blog data", author)
}

// ReadBlog
// @Summary      Detail Blog
// @Description  Detail Blog data
// @Tags         Blog
// @Accept       json
// @Produce      json
// @Success      200  {object}  models.BlogModel
// @Failure      400  {object}  httputils.HTTPError
// @Failure      401  {object}  httputils.HTTPError
// @Failure      404  {object}  httputils.HTTPError
// @Failure      500  {object}  httputils.HTTPError
// @Security     ApiKeyAuth
// @Param		 slug path string true "Slug"
// @Router       /blog/{slug} [post]
func ReadBlog(ctx *gin.Context) {
	slug := ctx.Param("slug")
	service := services.NewBlogService()
	blog, err := service.ReadService(slug)
	if err != nil {
		serverError(ctx, err.Error())
		return
	}

	successResponse(ctx, "Success get all blog data", blog)
}

func CreateBlog(ctx *gin.Context) {
	var body structs.CreateRequest
	if err := ctx.BindJSON(&body); err != nil {
		badRequest(ctx, err.Error())
		return
	}

	validator := validators.NewBlogValidator()
	if err := validator.ValidateCreateRequest(&body); err != nil {
		badRequest(ctx, err.Error())
		return
	}

	service := services.NewBlogService()
	data, err := service.CreateService(&body)
	if err != nil {
		serverError(ctx, err.Error())
		return
	}

	successResponse(ctx, "Succes Create Data", data)
}

func UpdateBlog(ctx *gin.Context) {
	id := ctx.Param("id")
	var body structs.CreateRequest
	if err := ctx.BindJSON(&body); err != nil {
		badRequest(ctx, err.Error())
		return
	}

	validator := validators.NewBlogValidator()
	if err := validator.ValidateCreateRequest(&body); err != nil {
		badRequest(ctx, err.Error())
		return
	}

	service := services.NewBlogService()
	data, err := service.UpdateService(&body, id)
	if err != nil {
		serverError(ctx, err.Error())
		return
	}

	successResponse(ctx, "Succes Update Data", data)
}

func DeleteBlog(ctx *gin.Context) {
	slug := ctx.Param("slug")

	validator := validators.NewBlogValidator()
	if err := validator.ValidateDeleteRequest(slug); err != nil {
		badRequest(ctx, err.Error())
		return
	}

	service := services.NewBlogService()
	if err := service.DeleteService(slug); err != nil {
		serverError(ctx, err.Error())
	}

	successResponse(ctx, "Success Delete Data", gin.H{})
}
