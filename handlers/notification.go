package handlers

import (
	"blog.example.com/v1/services"
	"blog.example.com/v1/structs"
	"github.com/gin-gonic/gin"
)


// SendNotification
// @Summary      Send Notification
// @Description  For logout
// @Tags         Notification
// @Accept       json
// @Produce      json
// @Param        body body structs.NotificationRequest true  "Body"
// @Success      200  {object}  structs.NotificationResponse
// @Failure      400  {object}  httputils.HTTPError
// @Failure      401  {object}  httputils.HTTPError
// @Failure      404  {object}  httputils.HTTPError
// @Failure      500  {object}  httputils.HTTPError
// @Router       /notification/send [post]
func SendNotification(ctx *gin.Context) {
	var body structs.NotificationRequest
	if err := ctx.BindJSON(&body); err != nil {
		serverError(ctx, err.Error())
	}

	err := services.SendNotification(body.Email, body.Message)
	if err != nil {
		serverError(ctx, err.Error())
	}

	successResponse(ctx, "success send notification", structs.NotificationResponse{})
}
