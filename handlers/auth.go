package handlers

import (
	"blog.example.com/v1/services"
	"blog.example.com/v1/structs"
	"github.com/gin-gonic/gin"
)

// Login
// @Summary      Login
// @Description  For authentication
// @Tags         Auth
// @Accept       json
// @Produce      json
// @Param        body body structs.LoginRequest true  "Body"
// @Success      200  {object}  structs.LoginResponse
// @Failure      400  {object}  httputils.HTTPError
// @Failure      401  {object}  httputils.HTTPError
// @Failure      404  {object}  httputils.HTTPError
// @Failure      500  {object}  httputils.HTTPError
// @Router       /auth/login [post]
func Login(ctx *gin.Context) {
	var body structs.LoginRequest
	if err := ctx.BindJSON(&body); err != nil {
		serverError(ctx, err.Error())
		return
	}

	service := services.NewAuthService()
	response, err := service.Login(body)
	if err != nil {
		serverError(ctx, err.Error())
		return
	}

	successResponse(ctx, "success login", response)
}

// LoginWithFirebase
// @Summary      Login with firebase
// @Description  For authentication using firebase
// @Tags         Auth
// @Accept       json
// @Produce      json
// @Param        body body structs.LoginRequest true  "Body"
// @Success      200  {object}  structs.LoginResponse
// @Failure      400  {object}  httputils.HTTPError
// @Failure      401  {object}  httputils.HTTPError
// @Failure      404  {object}  httputils.HTTPError
// @Failure      500  {object}  httputils.HTTPError
// @Router       /auth/login/firebase [post]
func LoginWithFirebase(ctx *gin.Context) {
	var body structs.LoginRequest
	if err := ctx.BindJSON(&body); err != nil {
		serverError(ctx, err.Error())
		return
	}

	service := services.NewAuthService()
	response, err := service.LoginWithFirebase(body)
	if err != nil {
		serverError(ctx, err.Error())
		return
	}

	successResponse(ctx, "success login", response)
}

// RefreshToken
// @Summary      Refresh JWT token
// @Description  For refresh jwt token auth
// @Tags         Auth
// @Accept       json
// @Produce      json
// @Param        body body structs.RefreshTokenRequest true  "Body"
// @Success      200  {object}  structs.LoginResponse
// @Failure      400  {object}  httputils.HTTPError
// @Failure      401  {object}  httputils.HTTPError
// @Failure      404  {object}  httputils.HTTPError
// @Failure      500  {object}  httputils.HTTPError
// @Router       /auth/refresh-token [post]
func RefreshToken(ctx *gin.Context) {
	var body structs.RefreshTokenRequest
	if err := ctx.BindJSON(&body); err != nil {
		serverError(ctx, err.Error())
		return
	}

	service := services.NewAuthService()
	response, err := service.RefreshToken(body.Token)
	if err != nil {
		serverError(ctx, err.Error())
		return
	}

	successResponse(ctx, "success login", response)
}

// Logout
// @Summary      Logout jwt
// @Description  For logout
// @Tags         Auth
// @Accept       json
// @Produce      json
// @Param        body body structs.LogoutRequest true  "Body"
// @Success      200  {object}  structs.Void
// @Failure      400  {object}  httputils.HTTPError
// @Failure      401  {object}  httputils.HTTPError
// @Failure      404  {object}  httputils.HTTPError
// @Failure      500  {object}  httputils.HTTPError
// @Router       /auth/logout [post]
func Logout(ctx *gin.Context) {
	var body structs.LogoutRequest

	if err := ctx.BindJSON(&body); err != nil {
		serverError(ctx, err.Error())
		return
	}

	service := services.NewAuthService()
	err := service.Logout(body.Token, body.RefreshToken)
	if err != nil {
		serverError(ctx, err.Error())
		return
	}

	successResponse(ctx, "success login", gin.H{})
}

func Register(ctx *gin.Context) {

}


