module blog.example.com/v1

go 1.16

require (
	firebase.google.com/go v3.13.0+incompatible // indirect
	firebase.google.com/go/v4 v4.7.1 // indirect
	github.com/gin-gonic/gin v1.7.7
	github.com/go-openapi/swag v0.21.1 // indirect
	github.com/go-playground/validator/v10 v10.10.0
	github.com/gobeam/mongo-go-pagination v0.0.8
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/joho/godotenv v1.4.0
	github.com/json-iterator/go v1.1.12
	github.com/klauspost/compress v1.14.4 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mitchellh/mapstructure v1.4.3 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/rogpeppe/go-internal v1.8.1 // indirect
	github.com/streadway/amqp v1.0.0 // indirect
	github.com/swaggo/files v0.0.0-20210815190702-a29dd2bc99b2
	github.com/swaggo/gin-swagger v1.4.1
	github.com/swaggo/swag v1.8.0
	github.com/xdg-go/scram v1.1.0 // indirect
	github.com/xdg-go/stringprep v1.0.3 // indirect
	go.mongodb.org/mongo-driver v1.8.3
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
	golang.org/x/tools v0.1.9 // indirect
	google.golang.org/api v0.40.0 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
