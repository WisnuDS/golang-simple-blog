package services

import (
	"blog.example.com/v1/models"
	"blog.example.com/v1/repositories"
	"blog.example.com/v1/structs"
	"go.mongodb.org/mongo-driver/bson"
)

type AuthorService struct {
	author *repositories.AuthorRepository
}

func NewAuthorService() *AuthorService {
	return &AuthorService{
		author: repositories.NewAuthorRepository(),
	}
}

func (a *AuthorService) ReadService(id string) (*[]bson.M, error) {
	author, err := a.author.GetById(id)
	if err != nil {
		return nil, err
	}

	return author, nil
}

func (a *AuthorService) CreateService(request *structs.AuthorCreateRequest) (*models.AuthorModel, error) {
	data, err := a.author.Create(
		request.Name,
		request.Description,
		request.Blogs,
		request.Email,
		request.Password,
	)

	if err != nil {
		return nil, err
	}

	return data, nil
}