package services

import (
	"blog.example.com/v1/configs"
	"blog.example.com/v1/repositories"
	"blog.example.com/v1/structs"
	"blog.example.com/v1/utils"
	"context"
	"fmt"
	"golang.org/x/crypto/bcrypt"
)

type AuthService struct {
	author *repositories.AuthorRepository
	token  *repositories.TokenBlockedRepository
}

func NewAuthService() *AuthService {
	return &AuthService{
		author: repositories.NewAuthorRepository(),
		token: repositories.NewTokenBlockedRepository(),
	}
}

func (a *AuthService) Login(request structs.LoginRequest) (*structs.LoginResponse, error) {
	email := request.Email
	password := request.Password

	user, err := a.author.GetByEmail(email)
	if err != nil {
		return nil, err
	}

	if user == nil {
		return nil, fmt.Errorf("User not found")
	}

	if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
		return nil, fmt.Errorf("Credential Not Match Our Record")
	}

	token, err := utils.GenerateToken(user, false)
	if err != nil {
		return nil, err
	}

	refreshToken, err := utils.GenerateToken(user, true)
	if err != nil {
		return nil, err
	}

	return &structs.LoginResponse{
		Token:        token,
		RefreshToken: refreshToken,
		User:         user,
	}, nil
}

func (a *AuthService) LoginWithFirebase(request structs.LoginRequest) (*structs.LoginResponse, error) {
	email := request.Email
	password := request.Password

	user, err := a.author.GetByEmail(email)
	if err != nil {
		return nil, err
	}

	if user == nil {
		return nil, fmt.Errorf("User not found")
	}

	if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
		return nil, fmt.Errorf("Credential Not Match Our Record")
	}

	claim := make(map[string]interface{})
	claim["User"] = user

	firebase := configs.GetFirebaseClient()
	token, err := firebase.CustomTokenWithClaims(context.Background(), user.FirebaseId, claim)
	if err != nil {
		return nil, err
	}

	return &structs.LoginResponse{
		Token: token,
		User:  user,
	}, nil
}

func (a *AuthService) RefreshToken(token string) (*structs.LoginResponse, error) {
	user, err := utils.ValidateToken(token)
	if err != nil {
		if err = a.token.Create(token); err != nil {
			return nil, err
		}

		return nil, err
	}

	newToken, err := utils.GenerateToken(user.User, false)

	return &structs.LoginResponse{
		Token: newToken,
		RefreshToken: token,
		User: user.User,
	}, nil
}

func (a *AuthService) Logout(token string, refreshToken string) error {
	if err := a.token.Create(token); err != nil {
		return err
	}

	if err := a.token.Create(refreshToken); err != nil {
		return err
	}

	return nil
}
