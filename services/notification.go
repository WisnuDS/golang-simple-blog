package services

import (
	"blog.example.com/v1/publisher"
	"blog.example.com/v1/structs"
)

func SendNotification(email string, message string) error {
	broker := publisher.NewPublisher()
	param := publisher.QueueParam{
		Name:       "testing",
		Durable:    false,
		AutoDelete: false,
		Exclusive:  false,
		NoWait:     false,
		Args:       nil,
	}
	queue, err := broker.CreateNewQueue(param)
	if err != nil {
		return err
	}


	err = broker.PublishMessage(queue.Name, structs.NotificationRequest{Email: email, Message: message})
	if err != nil {
		return err
	}

	return nil
}
