package services

import (
	"blog.example.com/v1/models"
	"blog.example.com/v1/repositories"
	"blog.example.com/v1/structs"
	"go.mongodb.org/mongo-driver/bson"
)

type BlogService struct {
	blog *repositories.BlogRepository
}

func NewBlogService() *BlogService {
	return &BlogService{
		blog: repositories.NewBlogRepository(),
	}
}

func (b *BlogService) ReadALlService(page string, limit string, sort string, dir string, search string) (*interface{}, error) {

	blogs, err := b.blog.GetAndPaginate(page, limit, sort, dir, search)
	if err != nil {
		return nil, err
	}

	return blogs, nil
}

func (b *BlogService) ReadService(slug string) (*models.BlogModel, error) {
	blog, err := b.blog.GetBlogBySlug(slug)
	if err != nil {
		return nil, err
	}

	return blog, nil
}

func (b *BlogService) ReadAllAndCount() (*[]bson.M, error) {
	author, err := b.blog.GetAllAndCount()
	if err != nil {
		return nil, err
	}

	return author, nil
}

func (b *BlogService) CreateService(request *structs.CreateRequest) (*models.BlogModel, error) {
	data, err := b.blog.Create(request.Title, request.Description, request.Author, request.Cover, request.Slug)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (b *BlogService) DeleteService(slug string) error {
	if err := b.blog.Delete(slug); err != nil {
		return err
	}

	return nil
}

func (b *BlogService) UpdateService(request *structs.CreateRequest, id string) (*models.BlogModel, error) {
	data, err := b.blog.Update(id, request.Title, request.Description, request.Author, request.Cover, request.Slug)
	if err != nil {
		return nil, err
	}

	return data, nil
}